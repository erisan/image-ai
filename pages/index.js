import React from "react"
import PageLayout from "../layouts/pageLayout";
import Link from "next/link"
import { useRouter } from 'next/router'
import styles from "../styles/Home.module.css";

export default function Home() {
  const router = useRouter()


  const projects = [
    {
      title: "Image Project",
      summary: "Teach based on images, from files or your webcam.",
      url: "/static/img/bg.jpg"
    },
    {
      title: "Image Project",
      summary: "Teach based on images, from files or your webcam."
    },
    {
      title: "Image Project",
      summary: "Teach based on images, from files or your webcam."
    }
  ]

  const [url, setUrl] = React.useState(null)


  const onImageChange = event => {
    alert("hello")
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      let imgObj = URL.createObjectURL(img)
      // console.log(imgObj , "imgObj")
      router.push({
        pathname: '/train/image',
        query: { src: imgObj },
      })


    }
  };


  return (
    <PageLayout>
      <div className="row w-100">


        <div className="mt-5 overflow-hidden">
        </div>
        <h1 className="mt-5">New Project</h1>
        <div>
          <label className="mt-5 input-label bg-white pointer d-inline-block shadow rounded py-3 px-5" id="fileId">
            <img src="/static/img/drive.png" className="me-2" style={{ width: 40 }} />   Open an existing project from file
           <input  onChange={onImageChange} type="file" style={{ opacity: 0 }} className="position-absolute " for="fileId" />
          </label>
        </div>

        <div className="col-12 overflow-hidden my-5"></div>

        {
          projects.map((data, index) => {
            return (
              <div key={index} className=" col-md mt-4">
                <div className="card">
                  <div className="card-img" style={{ backgroundColor: "gray", backgroundImage: `url(${data.url})` }}>

                  </div>
                  <div className="card-body mt-4">
                    <h3 className="card-title fw-bold my-3">Image Project</h3>
                    <p className="card-text">
                      Teach based on images, from files or your webcam.
    </p>
                    <Link href="/train/image">
                      <a href="#" className="btn btn-primary">
                        Continue
    </a>
                    </Link>

                  </div></div>
              </div>
            )
          })
        }
      </div>
    </PageLayout>
  );
}
