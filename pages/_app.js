import '../styles/globals.css'
import "bootstrap/dist/css/bootstrap.css";
import CookieConsent from "react-cookie-consent";


function MyApp({ Component, pageProps }) {
  return(
  <>
    <Component {...pageProps} /> <CookieConsent>This website uses cookies to enhance the user experience.</CookieConsent>
   
    </>
  )
}

export default MyApp
