import PageLayout from "../../../layouts/pageLayout";

export default function Home() {


  const classData = [
    {
      title: "Class 1",
      summary: "Add Images Samples",
    },
    {
      title: "Class 1",
      summary: "Add Images Samples",
    },
  ]





  return (
    <PageLayout>
      <div className="overflow-hidden my-3 w-100"></div>
      <div className="row w-100 ">


        <div className="col-md-6 pe-5">
          {
            classData.map((data, index) => {
              return (
                <div key={index} className="rounded  col-md-12 mt-4">
                  <div className="card rounded ">
                    <div className="card-body py-4 rounded shadow">
                      <h5 className="card-title fw-bold ">{data.title}</h5>
                      <hr />
                      <p className="card-text mt-3">
                        {data.summary}
                      </p>


                      <div className="upload-btn text-center">
                        <svg className="mt-3" width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path fill="#1967D2" fill-rule="evenodd" clip-rule="evenodd" d="M11 7.83L8.41 10.41L7 9L12 4L17 9L15.59 10.42L13 7.83V16H11V7.83ZM6 15H4V18C4 19.1 4.9 20 6 20H18C19.1 20 20 19.1 20 18V15H18V18H6V15Z"></path>
                        </svg>

                        <div>
                          Upload
</div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })
          }

        </div>

        <div className="col-md-3 pe-5 pt-5">
        <div className="overflow-hidden my-5">

        </div>

          <div className="rounded  col-md-12 mt-4">
            <div className="card rounded ">
              <div className="card-body py-4 rounded shadow">
                <h5 className="card-title fw-bold ">Traning</h5>

                <button className="btn btn-primary mb-2">Process Image</button>
                <hr />


              </div>
            </div>
          </div>

        </div>


        <div className="col-md-3">

          <div className="rounded  col-md-12 mt-4" >
            <div className="card rounded ">
              <div className="card-body py-4 rounded shadow">
                <h5 className="card-title fw-bold ">Preview</h5>

<hr className="my-4"/>
                <div className="camera-view">
          camera View
                </div>
                <hr />

                {/* <div>
                  You must train a model on the left before you can preview here
</div>

                <button className="btn btn-primary mb-2">View</button> */}

                <div className="mt-3">
                  <h5><b>Output</b></h5>

                </div>

                {
                  classData.map((d, i) => (
                    <div className="d-flex mt-4" key={i}>
                      <div className="me-2" style={{ whiteSpace: "nowrap" }}>
                        <b>  class {i + 1}</b>

                      </div>

                      <div className="progress w-100 mt-1">
                        <div
                          className={`progress-bar progress-bar-striped ${i ==0 ? null : "bg-warning"}`}
                          role="progressbar"
                          style={{ width: "50%" }}
                          aria-valuenow={10}
                          aria-valuemin={0}
                          aria-valuemax={100}
                        />

  50%
</div>

                    </div>
                  ))
                }



              </div>
            </div>
          </div>

        </div>
      </div>
    </PageLayout>
  );
}
