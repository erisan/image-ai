import React from "react";
import Footer from "./footer";
import Header from "./header";
import Layout from "./index";

export default function PageLayout({ children }) {
  return (
    <div>
      <Layout>
        <div className="col-md-11 mx-auto px-4">
          {/* header goes here */}
          <Header />
          <div></div>

          {/* content goes here */}
          {children}

          {/* footer goes here */}
          <div><Footer /></div>
        </div>
      </Layout>
    </div>
  );
}
