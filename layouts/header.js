import React from "react";
import Link from "next/link"

export default function Header() {

  const [showHeader, setShowHeader] = React.useState(false)
  return (
    <header className="w-100 header py-3">
      <div className="float-start pt-md-3 ps-3 pt-0 ">
        <div className="shadow bg-white px-4  alignYCeter py-3 rounded d-flex position-relative">
          <button className="headerBtn" onClick={() => setShowHeader(!showHeader)} onBlur={() => setShowHeader(!showHeader)}>

            <svg
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
              // xmlns:svgjs="http://svgjs.com/svgjs"
              version="1.1"
              style={{}}

              width={512}
              height={512}
              x={0}
              y={0}
              viewBox="0 0 384.97 384.97"
              style={{ enableBackground: "new 0 0 512 512", height: 30, width: "auto" }}
              xmlSpace="preserve"
              className="me-4"
            >
              <g>
                <g xmlns="http://www.w3.org/2000/svg">
                  <g id="Menu_1_">
                    <path
                      d="M12.03,120.303h360.909c6.641,0,12.03-5.39,12.03-12.03c0-6.641-5.39-12.03-12.03-12.03H12.03    c-6.641,0-12.03,5.39-12.03,12.03C0,114.913,5.39,120.303,12.03,120.303z"
                      fill="#222"
                      data-original="#000000"
                      style={{}}
                      className
                    />
                    <path
                      d="M372.939,180.455H12.03c-6.641,0-12.03,5.39-12.03,12.03s5.39,12.03,12.03,12.03h360.909c6.641,0,12.03-5.39,12.03-12.03    S379.58,180.455,372.939,180.455z"
                      fill="#222"
                      data-original="#000000"
                      style={{}}
                      className
                    />
                    <path
                      d="M372.939,264.667H132.333c-6.641,0-12.03,5.39-12.03,12.03c0,6.641,5.39,12.03,12.03,12.03h240.606    c6.641,0,12.03-5.39,12.03-12.03C384.97,270.056,379.58,264.667,372.939,264.667z"
                      fill="#222"
                      data-original="#000000"
                      style={{}}
                      className
                    />
                  </g>
                  <g></g>
                  <g></g>
                  <g></g>
                  <g></g>
                  <g></g>
                  <g></g>
                </g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
                <g xmlns="http://www.w3.org/2000/svg"></g>
              </g>
            </svg>

          </button>

          <h1 style={{ fontFamily: "Poppins" }}>
            <Link href="/">
              <a href="#" className="">
                Geomineral Imaging
    </a>
            </Link>

          </h1>

          <div className={`shadow col-md-3 menu position-absolute ${showHeader ? "show" : ""}`}>
            <ul className="list-unstyled pt-3">
              <li className="px-4 py-3 pointer">
              <img style={{width: "30px"}} className="me-2" src="/static/img/folder.png" />
              
                Menu 1
        </li>
              <li className="px-4 py-3 pointer">
              <img style={{width: "30px"}} className="me-2" src="/static/img/folder.png" />
                Menu 2
        </li>
              <li className="px-4 py-3 pointer">
              <img style={{width: "30px"}} className="me-2" src="/static/img/folder.png"/>
                Menu 3
        </li>
            </ul>

          </div>
        </div>

      </div>

    </header>
  );
}
